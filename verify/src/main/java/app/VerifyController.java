package app;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.ResponseEntity;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Value;

import static app.PaymentAction.TRANSFER;
import static app.PaymentAction.WITHDRAW;

@RestController
public class VerifyController {

    @Value("${paymenturl}")
    private String paymentUrl;

    @PostMapping("/")
    public String res(HttpServletRequest request) {
        String actionStr = request.getParameter("action");
        PaymentAction action = PaymentAction.from(actionStr);
        String amount = request.getParameter("amount");
        PaymentAmount paymentAmount = PaymentAmount.from(amount);
        if (action.equals(TRANSFER)) {
            System.out.println("Verify Controller: Going to transfer $"+amount);
            RestTemplate restTemplate = new RestTemplate();
            String fakePaymentUrl = this.paymentUrl;  //Internal fake payment micro-service
            ResponseEntity<String> response 
                = restTemplate.getForEntity(
                        fakePaymentUrl + "?action=" + actionStr + "&amount=" + paymentAmount.amount,
                        String.class);
            return response.getBody();
        } else if (action.equals(WITHDRAW)) {
            return "Verify Controller: Sorry, you can only make transfer";
        } else {
            return "Verify Controller: You must specify actionStr: transfer or withdraw";
        }
    }

}
