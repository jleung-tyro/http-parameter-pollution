package app;

public enum PaymentAction {
    TRANSFER("transfer"),
    WITHDRAW("withdraw");

    private String action;

    public static PaymentAction from(String actionStr) {
        if (actionStr == null) throw new RuntimeException("Unknown action");
        if (actionStr.equals("transfer")) return TRANSFER;
        else if (actionStr.equals("withdraw")) return WITHDRAW;
        else throw new RuntimeException("Unknown action");
    }

    PaymentAction(String action) {
        this.action = action;
    }
}
