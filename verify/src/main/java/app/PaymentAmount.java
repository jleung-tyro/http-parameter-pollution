package app;

import java.math.BigInteger;

public class PaymentAmount {
    public BigInteger amount;

    public static PaymentAmount from(String amount) {
        System.out.println(amount);
        return new PaymentAmount(amount);
    }

    public PaymentAmount(String amountStr) {
        BigInteger amount = BigInteger.valueOf(Long.parseLong(amountStr));
        this.amount = amount;
    }
}
